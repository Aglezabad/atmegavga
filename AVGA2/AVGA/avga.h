/*

    file:   avga.h
    desc:   Global header file. Include this one in your project.

    author: Jaromir Dvorak (md@unicode.cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
*/


#ifndef __AVGA__

#define __AVGA_VERSION_200__
#define __AVGA__

#include "core/video.h"
#include "core/driver.h"
#include "core/sound.h"

#include "utils/window.h"
#include "utils/overlay.h"
#include "utils/background.h"
#include "utils/delay.h"
#include "utils/misc.h"

#endif
