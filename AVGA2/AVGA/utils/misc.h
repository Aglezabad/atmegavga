/*

    file:   misc h
    desc:   miscellaneous useful inline functions

    author: Jaromir Dvorak (md@unicode cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
*/

#ifndef _MISC__H_
#define _MISC__H_


#define __device_reset()  	asm volatile("jmp 0")

static inline void strdump_P(unsigned char* dest, PGM_P src)
{
	while(1)
	{
		char c = pgm_read_byte(src++);
		if(!c) break;
		*dest++ = c;
	}
}


static inline void strdump(unsigned char* dest, char* src)
{
	while(1)
	{
		char c = *src++;
		if(!c) break;
		*dest++ = c;
	}
}

#endif
