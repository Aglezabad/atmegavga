/*

    file:   background.h
    desc:   BACKGROUND tool header.

    note:   This module provides basic block-reference drawing functions.
            Designed for drawing game maps, etc.

    todo:   Implement version for multi-block reference. (for example: one addres for 16*16 block)

    author: Jaromir Dvorak (md@unicode.cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef _BACKGROUND_H_
#define _BACKGROUND_H_

#include "../config_utils.h"

/* ########## BEGIN OF USER CONFIGURABLE SECTION ########## */
#ifndef BACKGROUND_UTILCONFIG
#define BACKGROUND_UTILCONFIG

//
//Which context to draw on (screen/selected window)
//
#define BACKGROUND_WINDOW

//
//Is the context vertically scrollable? I.e. does it contain one more row below the visible area?
//
#define BACKGROUND_SCROLLABLE

//
//Define which versions to compile.
//
#define BACKGROUND_DRAW
//#define BACKGROUND_DRAW_RLE
//#define BACKGROUND_DRAW_SIMPLE

//
//RLE decompressing macro.
//Upper 128 tiles can be addressed with this method.
//
#define RLE_get_next(img)                                           \
{                                                                   \
        if(cnt)  cnt--;                                             \
        else                                                        \
        {                                                           \
         byte = pgm_read_byte(img++);                               \
         if(byte & 0x80)  cnt = pgm_read_byte(img++);               \
         byte |= 0x80;                                              \
        }                                                           \
}

#endif
/* ########### END OF USER CONFIGURABLE SECTION ########### */


/*
 *
 * Function prototypes.
 *
 */

//
//Draws a raw (uncompressed) block image map img to window wnd with start left top block x, y in the selected window.
//Simplest version. does not handle any overflows.
//
void background_draw_simple   ( unsigned char x, unsigned char y, PGM_P img, unsigned char width, unsigned char height );

//
//Draws a raw (uncompressed) block image map img to window wnd with start left top block x, y in the selected window.
//Handles overflow - window and positioned image intersection is drawn only.
//Negative coords accepted.
//
unsigned char background_draw ( signed char x, signed char y, PGM_P img, unsigned char width, unsigned char height );

//
//Draws a RLE-compressed block image map img to window wnd with start left top block x, y in the selected window.
//Handles overflow - window and positioned image intersection is drawn only.
//Negative coords accepted.
//
void background_draw_RLE      ( signed char x, signed char y, PGM_P img, unsigned char width, unsigned char  height );

#endif

