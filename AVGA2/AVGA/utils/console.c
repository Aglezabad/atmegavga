/*

    file:   console.c
    desc:   implementation of console tools.

    author: Jaromir Dvorak (md@unicode.cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#include <avr/pgmspace.h>
#include <avr/eeprom.h>
#include <string.h>
#include "../core/driver.h"
#include "window.h"
//#include "console.h"
#define CONSOLE_WINDOW



static unsigned char curx=0, cury=0;

console_init()
{
	curx=0;
	cury=0;
}


void putch (char ch)
{

#ifdef CONSOLE_WINDOW
	register unsigned char endrow = window_get_height();
	register unsigned char *scraddr = window_get_ptr();
#else
	register unsigned char endrow = DRIVER_ROWS;
	register unsigned char *scraddr = driver_get_ptr();
#endif

	if(cury >= endrow) console_init();

	if(curx >= DRIVER_WIDTH || ch == '\n' || ch == '\r')
	{
		curx=0;
		cury++;
		if(cury >= endrow) //scroll up
		{
			unsigned char *ptr  = scraddr;
			unsigned char *ptr2 = ptr + DRIVER_WIDTH+HSCROLL;
			unsigned int i;

			for(i=0;i<((endrow-1)*(DRIVER_WIDTH+HSCROLL));i++) *ptr++=*ptr2++;

			memset(ptr,128,DRIVER_WIDTH+HSCROLL);
			cury--;
		}
	}



	if(ch>20)
	{
#ifdef CONSOLE_WINDOW
		window_set_block(curx, cury, ch);
#else
		driver_set_block(curx, cury, ch);
#endif
		curx++;
	}
}



console_init_stdio()
{
   fdevopen(putch, 0); //getch);
   console_init();
}