/*

    file:   config_screen.h
    desc:   Global screen configuration file.

    author: Jaromir Dvorak (md@unicode.cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
*/


#ifndef _CONFIG_SCREEN_
#define _CONFIG_SCREEN_

/*
 *
 * VIDEO SYNC GENERATOR screen configuration
 *
 */

#define VIDEO_SCREENCONFIG

//
// Scan each video line twice?
//
#define VIDEO_DOUBLE_SCANLINE

//
// Scan each video line four times?
//
//#define VIDEO_QUAD_SCANLINE


//
// Use window util for screen mapping?
//
#define VIDEO_USE_WINDOW

//
// Vertical sync enabled
//
#define VIDEO_ENABLE_VSYNC

//
// Allow changing vertical position in run-time
//
//#define VIDEO_DYNAMIC_YPOS

//
// Fine sync constant. Change this if there's a slight horizontal jitter in your picture.
//
#define VIDEO_FINE_SYNC		0

//
// Picture to screen offset.
// VIDEO_YPOS must be lower or equal than active picure line count in your video standard.
//
#define VIDEO_XPOS 75
#define VIDEO_YPOS 440  //456 pro 144
			//440  pro 200
/*
 *
 * VIDEO GRAPHICS DRIVER screen configuration
 *
 */

#define DRIVER_SCREENCONFIG


////
//Reference table size in tiles (without scrolling)
////
#define DRIVER_WIDTH                 21
#define DRIVER_HEIGHT                25

////
//Define how many upper blocks driver should look for in RAM
////
#define DRIVER_RAM_BLOCK_COUNT       26
////
//Define NATIVE TILE height in pixels here.
//note that DRIVER_BLOCK_WIDTH is always 8.
//The system DOES NOT SUPPORT other values.
////
#define DRIVER_BLOCK_HEIGHT          8

////
//Define if you want to enable horizontal scrolling
//note: if scrolling is not needed at all, a simple version of the driver should be used
////
#define HSCROLL			     	1

////
//Define how many extra rows you want to use for vertical scrolling
//(e.g.  how many windows can be independetly vertically scrollable)
////
#define VSCROLL                      2


#endif








