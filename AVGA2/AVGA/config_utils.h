/*

    file:   config_utils.h
    desc:   Global utils configuration file.

    author: Jaromir Dvorak (md@unicode.cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
*/

#ifndef _CONFIG_UTILS_
#define _CONFIG_UTILS_

/*
 *
 * WINDOW util configuration
 *
 */

#define WINDOW_UTILCONFIG

//
//Maximum count of windows on the screen.
//
#define MAX_WINDOWS		3

//
//define which types of scrolling to compile.
//
#define WINDOW_ABS_SCROLL
//#define WINDOW_SCROLL
//#define WINDOW_SCROLL_FULL

//
//enable this for ability to use individual tilest for each window.
//
#define WINDOW_INDIVIDUAL_TILESET

//
//enable this for ability to use individual tilest for each window.
//
//#define WINDOW_INDIVIDUAL_REFTABLE

//
//reset window on video_init?
//
#define WINDOW_RESET

//
//enable individual window syncing
//
//#define WINDOW_SYNC



/*
 *
 * OVERLAY util configuration
 *
 */

#define OVERLAY_UTILCONFIG

//
// define maximum block count for overlay graphics here.
// the system assumes that appropriate amount of allocated memory was given to driver_mmap(...)
//
#define OVERLAY_BLOCK_COUNT		DRIVER_RAM_BLOCK_COUNT	//use all available blocks

//
// Define this if you want the OVERLAY system to be able to remove drawn objects.
//
#define OVERLAY_CLEARABLE

//
//Which context to draw on (screen/selected window)
//
#define OVERLAY_WINDOW

//
// If this is defined, and there's not enough RAM to finish a drawing, eariler allocated blocks will be released.
//
//#define OVERLAY_INVERSE_PRIORITY

//
// Background color for overlay. If defined, the value is used as color to fill the RAM blocks background with.
//
//#define OVERLAY_BGCOLOR		4

//
// define if you want to compile functions for simple graphics primitives
//
#define OVERLAY_PRIMITIVES

//
// define if using overlay_draw.
//
#define OVERLAY_DRAW

//
// The image will be cut if it exceeds drawing area boundaries (clipping).
//
#define OVERLAY_DRAW_SAFE

//
// Theese constans are used only when OVERLAY_DRAW_SAFE is enabled
// They define restricted clipping margins of selected drawing area.
// If leftmargin and topmargin are nonzero, the given x, y coordinates are treated 
// relative to point at x=leftmargin, y=topmargin.
// If any of theese values is zero, operation regarding it is removed by the optimizer. 
// So no unnecesary waste of clock cycles.
//

#define OVERLAY_DRAW_LEFTMARGIN		0
#define OVERLAY_DRAW_RIGHTMARGIN	0
#define OVERLAY_DRAW_TOPMARGIN		0
#define OVERLAY_DRAW_BOTTOMMARGIN	0

//
// EOR all valid pixels of the image with lower 4 bits of param.
//
#define OVERLAY_DRAW_COLORMODIFY

//
// Allow the image transformations.
//
#define OVERLAY_DRAW_TRANSFORMATIONS

//
// transparent color for overlay_draw, pixels in the drawn image with this color will not change underlay pixels.
// If not defined, all source image pixels will be visible.
//
#define OVERLAY_DRAW_ALPHA		0



/*
 *
 * BACKGROUND util configuration
 *
 */

#define BACKGROUND_UTILCONFIG


//
//Which context to draw on (screen/selected window)
//
#define BACKGROUND_WINDOW

//
//Is the context vertically scrollable? I.e. does it contain one more row below the visible area?
//
#define BACKGROUND_SCROLLABLE

//
//Define which versions to compile.
//
#define BACKGROUND_DRAW
#define BACKGROUND_DRAW_RLE
//#define BACKGROUND_DRAW_SIMPLE

//
//RLE decompressing macro.
//Upper 128 tiles can be addressed with this method.
//
#define RLE_get_next(img)                                           \
{                                                                   \
        if(cnt)  cnt--;                                             \
        else                                                        \
        {                                                           \
         byte = pgm_read_byte(img++);                               \
         if(byte & 0x80)  cnt = pgm_read_byte(img++);               \
         byte |= 0x80;                                              \
        }                                                           \
}
#endif

