#define __ASM__
#define __SFR_OFFSET 0

#include <avr/io.h>
#include "driver.h"


//video pointers
.extern __ramp
.extern _pgmp
.extern _scrp

.extern _ptrtmp


//sync&signal generation
.extern c0
.extern line
.extern scroll
.extern wsync
.extern sp
.extern ss



; register usage
; http://www.nongnu.org/avr-libc/user-manual/FAQ.html#faq_reg_usage
#define r_pgm_bitmap_L  r8
#define r_pgm_bitmap_H  r9

#define c_arg0          r23


; r0, r1, X, Z
; X: 26 27
; Y: 28 29
; Z: 30 31

#define linecnt	r24
#define linecnh	r25

#define lineline r22

#define zero	r20
#define	ff	r21
#define tmp3    r19


//pixel register
#define tmp0     r16

//pixel register 2
#define	tmp2	 r18

//compare for high pointer register
#define compval  r17

//tileset pgm to ram difference
#define pgmramlo r6
#define pgmramhi r7

//16bit pointer to tile
#define ptr1lo  r2
#define ptr1hi  r3

//16bit pointer to next tile
#define ptr2lo  r4
#define ptr2hi  r5


//Z: temporary for reading pointers

//X: pointer to pointers





		   
;.global __vectors
;__vectors:

;	push	r0



//interrupt starts one line before first video line scanned

.global	TIMER1_COMPB_vect
TIMER1_COMPB_vect:

	push	r0
	in	r0, 0x3f	; sreg
	push	r0
	push	r1
	push	r2
	push	r3
	push	r4
	push	r5
	push	r6
	push	r7
	push	r8
	push	r9
	push	r10
	push	r11
	push	r12
	push	r13
	push	r14
	push	r15
	push	r16
	push	r17
	push	r18
	push	r19
	push	r20
	push	r21
	push	r22
	push	r23
	push	r24
	push	r25
	push	r26
	push	r27
	push	r30
	push	r31


        clr zero
        ser ff


	ldi lineline, 0
	ldi linecnt, 0x90
	ldi linecnh, 0x01



	; enable "scan break" interrupt
	ldi tmp0, 2
	sts TIMSK1, tmp0

	;clear pending flags
	out TIFR1, ff
	sei
	;wait for first int to come on
wait:	rjmp wait


endd: rjmp end


;nextln:	sbis TIFR1,1    ;cekat na TIFR1
;	rjmp nextln
;	;fine sync
;	lds tmp0,TCNT1L
;	sbrs tmp0,0
;	rjmp .



        ;sem jumpne irq...
.global	TIMER1_COMPA_vect       ;TODO: dat na zacatek line.
TIMER1_COMPA_vect:
	out VIDEO_PORT, zero
	;out TIFR1, ff       	; je to zde potreba? neni. flag se cisti automaticky na zacatku nebo az na konci interruptu?    na zacatku.
	pop tmp0 ;;destroy stack address stored by mcu when executing irq
	pop tmp0
	sei
	nop

	;out VIDEO_PORT,ff
	;out VIDEO_PORT, zero

	sbiw linecnt,1
        breq endd

	sbrs linecnt,0
        inc lineline



;mame 953 cyklu na linku



;ld
;mul
;add
;adc
;st
;st
;...

;ldi XL,0
;ldi XH,0




;now compute pointers


	 ;load scrp
         lds XL, _scrp
         lds XH, _scrp+1
         lds pgmramlo, __ramp
         lds pgmramhi, __ramp+1
         lds r_pgm_bitmap_L, _pgmp
         lds r_pgm_bitmap_H, _pgmp+1
         mov c_arg0, lineline



	 ; pricti k scrp DRIVER_COLUMNS + line/8, vysledkem je adresa radku v XH
         mov tmp3, c_arg0
         lsr tmp3
         lsr tmp3
         lsr tmp3
         ldi tmp2, DRIVER_COLUMNS
         mul tmp3, tmp2
         add XL, r0
         adc XH, r1

         
         sub pgmramlo, r_pgm_bitmap_L
         sbc pgmramhi, r_pgm_bitmap_H


	 ; pricti k pointerum ram a pgm (line&7)*4
	 andi c_arg0, 7
         lsl c_arg0
         lsl c_arg0
         clr r1
         add r_pgm_bitmap_L, c_arg0
         adc r_pgm_bitmap_H, r1


         ldi tmp3, DRIVER_BLOCK_SIZE	     ; velikost bloku

	 ; jak urcit hodnotu compval?
         ;
         ; v ptrhi je:
         ; 000x xxxx   xxx0 0000
         ; secteno s r_pgm_bitmap_H
         ;
         ;
         ;
         ;
         ; pocet tiles v ram musi byt nasobek 8
         ;
         ;
         


	 ; cislo od 0 do 32
         ldi compval, 30
         add compval, r_pgm_bitmap_H
         

         
         
         ;jak urcit pgmramhi:pgmramlo?
         ;
         ;
         ;
         ;
         ;pointer v ram = pointer v pgm + pgmramhi:pgmramlo
         ;pointer v ram - pointer v pgm = pgmramhi:pgmramlo
         ;







         lds ZL, _ptrtmp
         lds ZH, _ptrtmp+1




; a jedeme preloadovat
;;;PRELOAD
;prvni, druha a treti tile se budou preloadovat specialne
;prvni tile: rovnou se vypocita a zustane v Z
;druha tile: preload do ptr1 a po vygenerovani 1. tile dat do Z
;treti tile: preload do ptr2
;dal do RAM:


ldi tmp2, 1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         movw ptr1lo, r0

        ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         movw ptr2lo, r0


         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

        ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

       ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1


         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1

         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1
         
         ld tmp2, X+
         mul tmp2, tmp3
         add r0, r_pgm_bitmap_L
         adc r1, r_pgm_bitmap_H
         st Z+, r0
         st Z+, r1




	 ; ptr = buf :-)
         lds XL, _ptrtmp
         lds XH, _ptrtmp+1
         


nop
nop

nextln: lds tmp0,TCNT1L   ;wait for visible area
	cpi tmp0,190  ;190 pro 4cpp
	brlo nextln


; 1. tile (scrollovaci) se musi preloadnout za synthem, a tady budeme pouze cist z RAM, asi...


;skok muze byt po 1, po 3 a po 5. pixelu. (po 7 ne protoze tam musi bejt swap a movw)
;chceme po co nejvyssim pixelu?

; po 1: je pozde rozhodovat...

; (po 5 ne protoze testovaci intrukce musi bejt mezi 7 a 8. Tam je jediny misto na 1cyklovou ins.
; swapovat se skutecne musi mezi kazdym pixlem...



;OMEZENI pro ram2pgm: ram2pgm musi byt umistene tak aby v ramci renderovani te PGM tile (po te v RAM) to mohlo skocit na pgm2ram
;test na n-1 musi byt pred skokem
;store musi byt bezpodminecne nutne az po testu na pgm,  tedy pokud dalsi tile bude v RAM. jinak si vzdycky prepiseme registry
;add adc musi byt taky v RAM













/*
;rozhodnout jestli prvni tile je v RAM nebo v PGM


loop:
out

lpm
out

swap
rjmp loop
*/

;loop: rjmp loop


;zde: n+1 preloaded, n fetched




; tenhle zacatek je spatne.... musi se skocit nekam na ram2pgm nebo pgm2ram
	movw ZL, ptr1lo
	lpm tmp0, Z+

	out VIDEO_PORT,tmp0 			;1
	swap tmp0
	nop
	nop

pgm1:	out VIDEO_PORT,tmp0			;2

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;3

	swap tmp0
	ld ptr1lo,X+    ; load n+2
	out VIDEO_PORT,tmp0			;4

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;5

	ld ptr1hi,X+    ; load n+2
	swap tmp0
	out VIDEO_PORT,tmp0			;6

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;7

	swap tmp0
	movw ZL, ptr2lo ; fetch n+1			; movw -- musi bezpodminecne nutne byt mezi 7 a 8 pixelem PGM.
	cp ptr1hi, compval ; compare n+2		; TESTOVACI INSTRUKCE -- musi byt bezpodminecne nutne taky mezi 7 a 8
	out VIDEO_PORT,tmp0			;8

; n++ ================================================================================

	lpm tmp0,Z+                   	   	;//a jedeme novou tile (tile1) - ale co kdyz je v ram? - MUSIME MIT 16 pixlu, tedy 2 PGM tily v kodu...
	out VIDEO_PORT,tmp0 			;1

	swap tmp0
	brsh pgm2ram    ; n+1 v ram?
	nop
pgm2:	out VIDEO_PORT,tmp0			;2

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;3

	swap tmp0
	ld ptr2lo,X+        ; load n+2
	out VIDEO_PORT,tmp0			;4

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;5

	ld ptr2hi,X+        ; load n+2
	swap tmp0
	out VIDEO_PORT,tmp0			;6

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;7

	swap tmp0
	movw ZL, ptr1lo	;fetch n+1		; movw -- musi bezpodminecne nutne byt mezi 7 a 8 pixelem PGM.
	cp ptr2hi, compval  ;compare n+2	; TESTOVACI INSTRUKCE -- musi byt bezpodminecne nutne taky mezi 7 a 8
ram2pgm:out VIDEO_PORT,tmp0			;8

; n++ ==================================================================

	lpm tmp0,Z+                   	   	;//a jedeme novou tile (tile1) - ale co kdyz je v ram? - MUSIME MIT 16 pixlu, tedy 2 PGM tily v kodu...
	out VIDEO_PORT,tmp0 			;1

	swap tmp0
	brlo pgm1     ;n+1 v pgm?
	movw ptr1lo, ptr2lo   ;n+1 is in ptr1lo:ptr1hi
pgm2ram:out VIDEO_PORT,tmp0              	;2

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;3

	ld ptr2lo,X+        ; load n+2             ;kdy se komparuje tato tile????
	swap tmp0
	out VIDEO_PORT,tmp0			;4

	lpm tmp0,Z+
	out VIDEO_PORT,tmp0			;5

	ld ptr2hi,X+        ; load n+2
	swap tmp0
	out VIDEO_PORT,tmp0			;6

	lpm tmp0,Z+
ram:	out VIDEO_PORT,tmp0			;7

	swap tmp0
	movw ZL, ptr1lo ;fetch n
        add ZL, pgmramlo
	out VIDEO_PORT,tmp0			;8

; n++ ==================================================================

       	adc ZH, pgmramhi
	ld tmp2, Z+
	out VIDEO_PORT,tmp2			;1

	ld tmp0, Z+
	swap tmp2
	out VIDEO_PORT,tmp2			;2

       	movw ptr1lo, ptr2lo  ; store n+2 as n+1
	ld ptr2lo, X+        ; load n+2
	out VIDEO_PORT,tmp0			;3

	swap tmp0
	ld ptr2hi, X+        ; load n+2
	out VIDEO_PORT,tmp0			;4

	cp ptr1hi, compval      ; compare n+1
	ld tmp2, Z+
	out VIDEO_PORT,tmp2			;5

       	ld tmp0, Z+
	swap tmp2
	out VIDEO_PORT,tmp2			;6

	movw ZL, ptr1lo ;fetch n
	brsh  ram
	cp ptr2hi, compval  ;compare n+2
	out VIDEO_PORT,tmp0			;7

	swap tmp0
	rjmp ram2pgm









; 32MHz
;cela linka: 1016 cyklu, 812 visible

;1tile ma 4*8:32 cyklu
;22 tiles ma 32*22= 704 visible,  22*10












; cela linka: 953 cyklu  (762 visible)
; double buffering pro 176*200 pixelu:  3*176 = 528 cyklu visible
; zbyva 425 cyklu
; na 23 tiles je potreba 23*32=736 cyklu. Na radek:  368 cyklu. Zbyva 57 cyklu...



; cela linka: 953 cyklu  (762 visible)
; double buffering pro 200*150 pixelu:  3*200 = 600 cyklu visible
; zbyva 353 cyklu
; na 25 tiles je potreba 25*32=800 cyklu. Na radek: 368 cyklu zbyva. 266 cyklu potrebujeme. Pohoda



; @28MHz
; cela linka: 887 cyklu  (711 visible)
; double buffering pro 192*144 pixelu:  3*200 = 600 cyklu visible
; zbyva 287 cyklu
; na 25 tiles je potreba 25*32=800 cyklu. Na radek: 287 cyklu zbyva. 266 cyklu potrebujeme....



end:    ;wsync = 1;
	ldi tmp0,1
	sts wsync, tmp0

	;__video_timer_select_OVF_IRQ();
	sts TIMSK1, tmp0
	
	;__video_timer_clear_OVF_IRQ();
	out TIFR1, ff

//within the next line
epilog:	pop	r31
	pop	r30
	pop	r27
	pop	r26
	pop	r25
	pop	r24
	pop	r23
	pop	r22
	pop	r21
	pop	r20
	pop	r19
	pop	r18
	pop	r17
	pop	r16
	pop	r15
	pop	r14
	pop	r13
	pop	r12
	pop	r11
	pop	r10
	pop	r9
	pop	r8
	pop	r7
	pop	r6
	pop	r5
	pop	r4
	pop	r3
	pop	r2
	pop	r1
	pop	r0
	out	0x3f, r0	; 63
	pop	r0
	reti






