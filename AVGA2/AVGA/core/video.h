/*

    file:   sync.h
    desc:   SYNC GENERATOR header

    note:   Pixel period is 5 clock cycles. For PAL: @20MHz, it gives 208 pixels per active picture period (52 uS).
            One frame has 288 lines. When doubling each line, a very nice 192*144 (4:3) resolution is achieved.
            This resolution is also dividable by 8, so we can use 8*8 sized blocks.
            Desired xtal: 19.6608MHz.

    author: Jaromir Dvorak (md@unicode.cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/


#ifndef _VIDEO_H_
#define _VIDEO_H_

#include "../config_hw.h"
#include "../config_screen.h"


/* ########## BEGIN OF USER CONFIGURABLE SECTION ########## */
#ifndef VIDEO_HWCONFIG
#define VIDEO_HWCONFIG

//
// Sync generator needs one full AVR 16-bit timer/counter with PWM.
// configured here for TIMER1, atmega168.
//
#define VIDEO_TIMER_CMP_IRQ         TIMER1_COMPA_vect
#define VIDEO_TIMER_OVF_IRQ         TIMER1_OVF_vect
#define VIDEO_TIMER_OCRA            OCR1A
#define VIDEO_TIMER_OCRB            OCR1B
#define VIDEO_TIMER_ICR             ICR1
#define	VIDEO_TIMER_OCRB_DDR        DDRB	//hsync DDR
#define VIDEO_TIMER_OCRB_PIN        2		//hsync PIN
#define	VIDEO_VSYNC_DDR		        DDRB	//vsync DDR
#define VIDEO_VSYNC_PIN     		1		//vsync PIN
#define	VIDEO_VSYNC_PORT	        PORTB	//vsync PORT

//
// Setting up the timer: fast PWM, no prescaling, OCRB connected to SYNC.
//
#define __video_timer_select_OVF_IRQ()      TIMSK1 = 1 << TOIE1
#define __video_timer_select_CMP_IRQ()      TIMSK1 = 1 << OCIE1A
#define __video_timer_select_CMPB_IRQ()     TIMSK1 = 1 << OCIE1B
#define	__video_timer_clear_OVF_IRQ()       TIFR1 = 0xFF
#define __video_timer_clear_CMP_IRQ()       TIFR1 = 0xFF
#define __video_timer_setup_mode14()							\
	TCCR1A = 1 << WGM11 | 0 << WGM10 | 1 << COM1B1 | 1 << COM1B0;			\
	TCCR1B = 1 << WGM13 | 1 << WGM12 | 0 << CS12 | 0 << CS11 | 1 << CS10;

//
// Scan-line interrupt will occur between 2 instructions.
// Since not all AVR instructions take only one cycle, precission sync is needed to avoid jitter.
// 16-bit PC (<=64k) AVR devices have instructions with maximum of four clock cycles.
// This macro synchronises the callers program flow with video timer (modulo 4 = 0).
// We must also consider few cycles waiting to do the sync at right time,
// depending on amount of cycles consumed by C code since interrupt occured.
// configured here for TIMER1, atmega168.
//

/*
#define VIDEO_TIMER_FINE_SYNC()					\
asm volatile("rjmp .  					\r\n	\
			  lds __tmp_reg__, %0		\r\n	\
			  sbrs __tmp_reg__, 0		\r\n	\
			  rjmp .					\r\n 	\
			  sbrs __tmp_reg__, 1		\r\n	\
			  lpm __tmp_reg__, Z		\r\n"	: : "M" (_SFR_MEM_ADDR(TCNT1L)))
*/



//
// Simple version. Does only 0/1 cycle sync. It will work properly only when all the lines are scanned
// during the blocking call of video_sync() function, becouse only one and two cycle instructions are used there.
//

#define VIDEO_TIMER_FINE_SYNC()					\
asm volatile("lds __tmp_reg__, %0		\r\n	\
			  sbrs __tmp_reg__, 0		\r\n	\
			  rjmp .					\r\n"  : : "M" (_SFR_MEM_ADDR(TCNT1L)))


#endif
#ifndef VIDEO_SCREENCONFIG
#define VIDEO_SCREENCONFIG

//
// Scan each line twice?
//
#define VIDEO_DOUBLE_SCANLINE

//
// Scan each video line four times?
//
#define VIDEO_QUAD_SCANLINE

//
// Use window util for screen mapping?
//
#define VIDEO_USE_WINDOW

//
// Vertical sync enabled
//
#define VIDEO_ENABLE_VSYNC

//
// Allow changing vertical position in run-time
//
#define VIDEO_DYNAMIC_YPOS

//
// Fine sync constant. Change this if there's a slight horizontal jitter in your picture.
//
#define VIDEO_FINE_SYNC		0

//
// Picture to screen offset.
//
#define VIDEO_XPOS 100 //80 for TV	//116 for sony PS One LCD screen
#define VIDEO_YPOS 480

#endif
/* ########### END OF USER CONFIGURABLE SECTION ########### */



//
// CPU clock frequency
//
#ifdef F_CPU
#define CPU_MHZ					((float)F_CPU/1000000)
#else
#define CPU_MHZ                 (20.0)
#warning Using 20.0 MHz as default CPU clock.
#endif

//
// Picture to screen offset
//
#ifdef VIDEO_DYNAMIC_YPOS
unsigned int video_ypos;
#else
#define video_ypos VIDEO_YPOS
#endif



/*
 *
 * Video signal timings.
 *
 *
 * PAL standard:
 *
 * V-Sync timings
 *  DEP  __________            ___
 *    | |                 DFP |IFP|
 *    |_|    IEP       _______|   |
 *
 *
 * H-Sync timings
 *                |\/||/\//\|
 *            BP  |    AP   | FP
 * _        ______|         |__
 *  |  SP  |
 *  |______|
 *
 * http://www.rickard.gunee.com/projects/video/sx/vsync_big.png
 * http://www.kolumbus.fi/pami1/video/pal_ntsc.html
 *
 */



//
// Vertical sync mark
//

#define GVSYNC		  0x8000


// VGA timings (industry standard:  31.46875 kHz vertical, 25.175 MHz pixel clock)
#define VGA_LP        (int)(31.775*CPU_MHZ)   // Nominal line period 31.775 �s
#define VGA_SP        (int)(3.8133*CPU_MHZ)    // Synchronizing pulse 3.8133 �s

// SVGA timings (37.87 kHz vertical, 40.0 MHz pixel clock)
#define SVGA_LP        (int)(26.4*CPU_MHZ)   // Nominal line period 26.4 �s
#define SVGA_SP        (int)(3.2*CPU_MHZ)    // Synchronizing pulse 3.2 �s

// PAL timings
#define PAL_LP        (int)(64*CPU_MHZ)   // Nominal line period 64 �s
#define PAL_DEP       (int)(2.35*CPU_MHZ) // Duration of equalizing pulses 2.35 �s
#define PAL_DFP       (int)(27.3*CPU_MHZ) // Duration of field-synchronizing pulses 27.3 �s
#define PAL_SP        (int)(4.7*CPU_MHZ)  // Synchronizing pulse 4.7 �s

// NTSC timings
#define NTSC_LP       (int)(63.5555*CPU_MHZ) // Nominal line period 63.5555 �s
#define NTSC_DEP      (int)(2.3*CPU_MHZ)  	 // Duration of equalizing pulses 2.3 �s
#define NTSC_DFP      (int)(27.1*CPU_MHZ) 	 // Duration of field-synchronizing pulses 27.1 �s
#define NTSC_SP       (int)(4.7*CPU_MHZ)  	 // Synchronizing pulse 4.7 �s



//
//VGA pattern, 640x350@70Hz
//
#define VGA1_TIMINGS  { {VGA_LP, VGA_SP, 447},        			\
           	    	  {VGA_LP, VGA_SP, 2 | GVSYNC}, {0} }
//
//VGA pattern, 640x480@60Hz
//
#define VGA2_TIMINGS  { {VGA_LP, VGA_SP, 523},        			\
	          		    {VGA_LP, VGA_SP, 2 | GVSYNC}, {0} }
//
//SVGA pattern, 800x600@56Hz
//
#define SVGA_TIMINGS  { {SVGA_LP, SVGA_SP, 624},        		\
	           		    {SVGA_LP, SVGA_SP, 4 | GVSYNC}, {0} }


//
// PAL pattern
//
#define PAL_TIMINGS { {PAL_LP/2, PAL_DFP, 5},       \
                      {PAL_LP/2, PAL_DEP, 5},       \
                      {PAL_LP, PAL_SP, 1},          \
                      {PAL_LP, PAL_SP, 304},        \
                      {PAL_LP/2, PAL_DEP, 5},       \
                      {PAL_LP/2, PAL_DFP, 5},       \
                      {PAL_LP/2, PAL_DEP, 4},       \
                      {PAL_LP, PAL_DEP, 1},         \
                      {PAL_LP, PAL_SP, 304},        \
                      {PAL_LP/2, PAL_SP, 1},        \
                      {PAL_LP/2, PAL_DEP, 5}, {0} }

//
// NTSC pattern
//
#define NTSC_TIMINGS { {NTSC_LP/2, NTSC_DEP, 6},       \
                       {NTSC_LP/2, NTSC_DFP, 6},       \
                       {NTSC_LP/2, NTSC_DEP, 6},       \
                       {NTSC_LP,   NTSC_SP, 253},      \
                       {NTSC_LP/2, NTSC_SP, 1},        \
                       {NTSC_LP/2, NTSC_DEP, 6},       \
                       {NTSC_LP/2, NTSC_DFP, 6},       \
                       {NTSC_LP/2, NTSC_DEP, 5},       \
                       {NTSC_LP,   NTSC_DEP, 1},       \
                       {NTSC_LP,   NTSC_SP, 253},  {0} }


typedef struct
{
	unsigned int period;	// T (t1+t2)
	unsigned int width;		// t1 (low part)
	unsigned int times;		// times
} const sync_t;





#include <avr/pgmspace.h>
#define VGA1 ({ static sync_t PROGMEM __c[] = VGA1_TIMINGS; __c; })
#define VGA2 ({ static sync_t PROGMEM __c[] = VGA2_TIMINGS; __c; })
#define SVGA ({ static sync_t PROGMEM __c[] = SVGA_TIMINGS; __c; })
#define PAL  ({ static sync_t PROGMEM __c[] = PAL_TIMINGS;  __c; })
#define NTSC ({ static sync_t PROGMEM __c[] = NTSC_TIMINGS; __c; })





/*
 *
 * Function prototypes.
 *
 */

//
// Call for initialize the generator. Desired signal is given as argument.
//
void video_init(sync_t *s);

//
// Waits for retrace.
// Returns amount of time spent here.
//
unsigned int video_sync(void);
#define sync() video_sync()

//
// Enable/disable generator.
//
#define video_enable()  asm volatile("sei")
#define enable()        video_enable()
#define video_disable() asm volatile("cli")
#define disable()       video_disable()


//
// Adjust picture to screen offset
//
#define video_set_xpos(val)	 	({ VIDEO_TIMER_OCRA = 928; /*val; /*(val & ~3) | (VIDEO_FINE_SYNC & 3);*/ })
#define video_set_ypos(val)		({ video_ypos = val; })



/*
 *
 * Basic effects support (when not using window util)
 *
 */

//
//color effects
//
#define video_show()           ({ extern unsigned char video_scroll; video_scroll |= 0x08;  })
#define video_hide()           ({ extern unsigned char video_scroll; video_scroll &= ~0x08; })
void video_set_colormask(unsigned char mask);

//
//scrolling effects
//
void video_set_scroll(unsigned char x, unsigned char y);
void video_scroll(signed char x, signed char y);
#define video_adjust_for_background(x, y) video_set_scroll(~(x), ~(y))

//
//placement modification
//
#define video_move(n)          ({ extern unsigned char video_line; video_line += n; })
#define video_set_startline(n) ({ extern unsigned char video_line; video_line = n; })


#endif /* _VIDEO_H_ */
