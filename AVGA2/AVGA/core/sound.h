/*

    file:   sound h
    desc:   Sound sequencer header 

    author: Jaromir Dvorak (md@unicode cz)

    This file is part of the AVGA platform.
    http://avga.prometheus4.com/


    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
 
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
  
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 
*/


#ifndef _SOUND_H_
#define _SOUND_H_

#include "../config_hw.h"
#include "../config_sound.h"



/* ########## BEGIN OF USER CONFIGURABLE SECTION ########## */
#ifndef SOUND_HWCONFIG
#define SOUND_HWCONFIG

//
// not supported yet.
//
//#define SOUND_ENABLE_SYNTESIZER

//
// audio output port
//
#define SOUND_TIMER_PORT		PORTD
#define SOUND_TIMER_DDR			DDRD
#define SOUND_TIMER_DDR_PIN		3


//
// audio timer setup
//
// configured here for TIMER2, ATMega168
//
// Note: The m168 8bit timer version has two output pins. We use one value for TOP (OCR2A)
// and the second (OCR2B) as compare value set to 1 in CTC mode. So if OCR2A is set to 0, 
// there is no output at OCR2B pin (no sound) without the need of stopping the timer at all.
//
#define __sound_init_timer()	OCR2B = 1; 		\
								TCCR2A = 0x12;	\
								TCCR2B = 0x05;

#define __sound_set_timer(a) ({ OCR2A = a; })

#define __sound_stop_timer()    OCR2A = 0

//
// example for 8bit timers with only one output pin
//
//#define __sound_init_timer()
//#define __sound_set_timer(a) ({ OCR2 = a; if(a) TCCR2 = 0x1D; else TCCR2 = 0; })
//#define __sound_stop_timer()    TCCR2 = 0
//


#endif
#ifndef SOUND_SOUNDCONFIG
#define SOUND_SOUNDCONFIG

//
// global enable/disable sound
//
#define SOUND_ENABLE

#endif
/* ########### END OF USER CONFIGURABLE SECTION ########### */



#ifdef SOUND_ENABLE

#ifdef SOUND_ENABLE_SYNTHESIZER
#define SOUND_LINEHANDLE
#else
#define SOUND_FRAMEHANDLE
#endif


//
//prototypes
//

void inline sound_frame_handle();
void inline sound_line_handle();

void sound_init();
void sound_play(PGM_P sndfile);
void sound_stop();


//
//compatibility
//
#define audio_init()			sound_init()
#define audio_play(sndfile) 	sound_play(sndfile)
#define sound_stop()			sound_stop()


#endif
#endif

