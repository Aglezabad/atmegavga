#include <avr/io.h>
#include <stdio.h>
#include <avr/pgmspace.h>
//#include "AVGA/avga.h"


static const unsigned char codes[] PROGMEM = {0x70, 0x69, 0x72, 0x7a, 0x6b, 0x73, 0x74, 0x6c, 0x75, 0x7d, 0x1c, 0x32, 0x21, 0x23, 0x24, 0x2b, 0x34, 0x33, 0x43, 0x3b, 0x42, 0x4b, 0x3a, 0x31, 0x44, 0x4d, 0x15, 0x2d, 0x1b, 0x2c, 0x3c, 0x2a, 0x1d, 0x22, 0x35, 0x1a, 0x16, 0x1e, 0x26, 0x25, 0x2e, 0x36, 0x3d, 0x3e, 0x46, 0x45, 0x29, 0x41, 0x49, 0x4a,0x5A};
static const unsigned char chars[] PROGMEM = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ' ', ',', '.', '/', 13};
char keyboard_scan2key(unsigned char code)
{
	unsigned char* ptr PROGMEM = codes;
	unsigned char i;
	for(i=0;i<sizeof(codes);i++)
	{
	 	if(pgm_read_byte(ptr++) == code) return pgm_read_byte(&chars[i]);
	}
	return 0;
}









static void (*kbevent)(unsigned char code, unsigned char flags) = 0;

unsigned char *_kbuf;
unsigned char _kcnt;

keyboard_handle()
{
	static unsigned char flags=0;
	unsigned char kbuf[8];
	unsigned char i,kcnt;

	asm volatile("cli");
	kcnt=_kcnt;
	_kcnt=0;
	memcpy(kbuf,_kbuf,sizeof(kbuf));
	asm volatile("sei");

	for(i=0;i<kcnt;i++)
	{
       		switch(kbuf[i])
		{
			case 0xE0:	// special key
					flags |= 1;
					break;
      			case 0xF0:	// key release
	      				flags |= 2;
					break;
	      		default:	// key press
					if(kbevent)  (kbevent)(kbuf[i],flags);
					flags=0;
	      	}
	}
}





/*void onKbEvent(unsigned char code, unsigned char flags)
{

	if(!flags)
	{
		unsigned char key = kbGetScanCode(code);
		if(key) putch(key);
		else printf_P(PSTR("Unknown %d\n"),code);
	}

	if(flags & 1)
	{
		if(flags & 2) printf_P(PSTR("release "));
		printf_P(PSTR("Special %d\n"),code);
	}
}
*/




/*

left:   107
right:  116
top:    117
bottom: 114

right ctrl: 116
right alt:  17

left ctrl: 20
left alt:  17




*/


int kbhit (void)
{
	return _kcnt;
}


static unsigned char kbuf[8];
void keyboard_init(void* cb)
{
   _kbuf=kbuf;
   kbevent=cb;

   DDRD &= ~0x01;  //rxd
   DDRD &= ~0x02;  //txd

   DDRD &= ~0x04;  //xck - pro hold down...
   DDRD &= ~0x10;  //xck

   UBRR0  = (/*16934400*/8000000/115200)/16 - 1;

   UCSR0B = 0x10;   //rxen & !txen
   UCSR0C = 0x46;   //synchronous USART, 8bit   -- bit 0. je CPOL
}




