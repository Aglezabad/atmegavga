


#define SPI_transfer_byte(byte)       ({ SPDR = byte; while(!(SPSR & _BV(SPIF))); SPDR; })
#define SPI_get_speed(x)     ((SPCR&3))
#define SPI_set_speed(x)     ({unsigned char __c=SPCR; __c&=~3; __c|=x&3; SPCR=__c;})
#define SPI_double_speed()   ({SPSR |=  0x01; })
#define SPI_normal_speed()   ({SPSR &= ~0x01; })
#define SPI_init()           ({DDRB |= 0x2C; DDRB &= ~0x10; PORTB |= 0x10; SPCR = 0x50;})



