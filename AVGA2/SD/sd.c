#include <avr/io.h>
#include "sd.h"
#include "spi.h"

#include <stdio.h>
#include <avr/pgmspace.h>

//sd card initialize
void SD_Init(void)
{
	SPI_init();
	SD_CS_Init();
	SD_CS_Deassert();
}

//sd send command
static inline unsigned char SD_SendCommand(uint8 cmd, uint32 arg)
{
	uint8 retry;
	for(retry=0;retry<6;retry++)	SPI_transfer_byte(0xff);  //TODO: musi to tu bejt?

	SD_CS_Assert();

	SPI_transfer_byte(cmd | 0x40);	//send command
	SPI_transfer_byte(arg>>24);
	SPI_transfer_byte(arg>>16);
	SPI_transfer_byte(arg>>8);
	SPI_transfer_byte(arg);
	SPI_transfer_byte(0x95);

	uint8 r1;
	retry = 100;
	while((r1 = SPI_transfer_byte(0xff)) == 0xff)	//wait response
	{
		//putch('.');
		if(!--retry) break;			//time out error
	}

	SD_CS_Deassert();

	return r1;				//return state
}

//reset sd card (software)
uint8 SD_Reset(void)
{
	uint8 i;
	uint16 retry=0;
	uint8 r1=0;

	SPI_normal_speed();
	//SPI_set_speed(0);
	//SPI_disable_IRQ();

	retry = 1000;

	do
	{
		for(i=0;i<100;i++) SPI_transfer_byte(0xff);
		r1 = SD_SendCommand(0, 0);	//send idle command
		if(!--retry) return 1;		//time out
	} 
	while(r1 != 0x01);

	retry = 20000;
	do
	{
		r1 = SD_SendCommand(1, 0);	//send active command
		if(!--retry) return 2;	//time out
	}
	while(r1);
	

	SPI_double_speed();
	r1 = SD_SendCommand(59, 0);	//disable CRC

	r1 = SD_SendCommand(16, 512);	//set sector size to 512
	return 0;	//normal return
}


//read one sector
uint8 SD_ReadSector(uint32 sector, uint8* buffer)
{
	//SPI_transfer_byte(0x55);
	//return;


	unsigned char r1 = 0xFF;
	uint16 i, retry=0;

//	SPI_set_speed(0);
//	SPI_double_speed();

	retry = 100;

	do
	{
		r1 = SD_SendCommand(17, sector<<9);	//read command
		if(!--retry) return 1;		//time out
	}
	while(r1 != 0x00);

	//printf_P(PSTR("%d "), retry);

	SD_CS_Assert();

        //wait to start recieve data
	retry=10000;
	while(SPI_transfer_byte(0xff) != 0xfe)
		if(!--retry)
		{
			SD_CS_Deassert();
			return 2;			//time out
		}

	//printf_P(PSTR("%d "), retry);

	//i=512+1;
	i = 512+1;
	while(--i)
	{
		unsigned char d = SPI_transfer_byte(0xff);
		if(i>505) *buffer++ = d;
	}

	SPI_transfer_byte(0xff);//crc
	SPI_transfer_byte(0xff);
	SD_CS_Deassert();

	return 0;
}








