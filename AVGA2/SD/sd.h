#ifndef __MMC_SD_h__
#define __MMC_SD_h__

#define uint8  unsigned char
#define  int8    signed char
#define uint16 unsigned int
#define  int16   signed int
#define uint32 unsigned long
#define  int32   signed long



#define SD_PORT       PORTB
#define SD_DDR        DDRB
#define SD_CS_PIN     0


#define SD_CS_Init()   	 { SD_DDR  |= _BV(SD_CS_PIN);  }
#define SD_CS_Assert()   { SD_PORT &= ~_BV(SD_CS_PIN); }
#define SD_CS_Deassert() { SD_PORT |=  _BV(SD_CS_PIN); }



void SD_Init(void);
uint8 SD_Reset(void);
uint8 SD_ReadSector(uint32 sector, uint8* buffer);

#endif
