
#include <stdio.h>

struct level_header
{
  unsigned int plane0ptr;
  unsigned int plane1ptr;
  unsigned int plane2ptr;
  unsigned short plane0length;
  unsigned short plane1length;
//  unsigned short plane2length;
  unsigned short width;
  unsigned short height;
  char name[16];

} __attribute__((packed));



unsigned short rle_get(FILE* f)
{
    static unsigned short rep=0, val=0;

    if(rep)    rep--;
    else
    {
        fread(&val, 1, sizeof(val), f);
    	if(val == 0xABCD)
	{
    		fread(&rep, 1, sizeof(rep), f);
    		fread(&val, 1, sizeof(val), f);
    		if(rep) rep--;
    	}
    }

    return val;
}



struct line_header
{
  unsigned short magic;
  unsigned short val1;
  unsigned short val2;
} __attribute__((packed));

main()
{
  FILE *f = fopen("MAPTEMP.CK4", "rb");
  
  struct level_header h;

  
  char magic[16];
  fread(magic,1,10, f);
//  magic[9]=0;

  if(strcmp(magic, "TED5v1.0."))
  {
    printf("Invalid input file!");
    return;
  }

  //printf("%s\n",magic);


  // difference of plane 1 is 1207

  //nase            keenext
  //0655h (1621) -> 2828
  //03CDh (973)  -> 2180

  int m;
  for(m=0;m<3;m++)
  {

  fread(&h, 1, sizeof(h), f);
  printf("\nProcessing map: %s\n",h.name);
  printf("width:%d\n",h.width);
  printf("height:%d\n",h.height);
  //printf("%d\n", h.plane0ptr); /// - what is this?


  char tt[65536];
  int i,j,k;
  for(k=0;k<3;k++)
  {
   unsigned short tmp; //1F74
   fread(&tmp, 1, sizeof(tmp), f);

   memset(tt,0,sizeof(tt));

   
   char name[64];
   sprintf(name,"%s%dx%d_plane%d.bin", h.name, h.width, h.height, k);
   FILE *fout = fopen(name,"wb");

   unsigned short bigbuf[0xFFFF];
   unsigned short kk=0;

   for(j=0;j<h.height;j++)
   {
    //printf("\n\n");
    for(i=0;i<h.width;i++)
    {
     unsigned short tile = rle_get(f);


     if(k==1 && tile) tile+=1206;

     fwrite(&tile, 1, sizeof(tile), fout);

     tt[tile] = 1;
     bigbuf[kk++] = tile;



     //printf("%02X ",tile);
    }
   }

   fclose(fout);


   sprintf(name,"%s%dx%d_8indices%d.bin", h.name, h.width, h.height, k);
   fout = fopen(name,"wb");
   j=0;
   for(i=0;i<sizeof(tt);i++)
   {
    putc(tt[i],fout);
    if(tt[i])
    {
      tt[i] = j;
      //unsigned short tile=i;
      //fwrite(&tile, 1, sizeof(tile), fout);
      j++;
    }
   }

   printf("Different tiles in plane %d: %d\n", k, j);
   fclose(fout);
   


   kk=0;
   sprintf(name,"%s%dx%d_8plane%d.bin", h.name, h.width, h.height, k);
   fout = fopen(name,"wb");
   for(j=0;j<h.height;j++)
    for(i=0;i<h.width;i++)
     putc(tt[bigbuf[kk++]],fout);

   fclose(fout);

  }
  unsigned short tmp; //1F74
  fread(&tmp, 1, sizeof(tmp), f);
  printf("dummy: %02X\n",tmp);


  }


}




  
