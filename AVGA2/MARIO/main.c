#include <stdio.h>
#include <string.h>
#include <avr/pgmspace.h>
#include "../AVGA/avga.h"


#include "../SD/sd.h"


//grahical files
#include "graphics/tileset.h"
#include "graphics/mario16.h"  //generated with x2h_rle
#include "graphics/map1.h"     //generated with x2h


//user graphical objects
static const unsigned char anim1[] PROGMEM = MARIO16;
static const unsigned char map1[] PROGMEM = MAP1;

//sound files
#include "sounds/music1.h"
#include "sounds/sounds.h"

const unsigned char pgmmap[] PROGMEM = TILESET;


//user sound objects
const unsigned char snd_jump[] PROGMEM = SND_JUMP;
const unsigned char snd_tram[] PROGMEM = SND_TRAM;
const unsigned char snd_impact[] PROGMEM = SND_IMPACT;
const unsigned char snd_fall[] PROGMEM = SND_FALL;





//memory for the driver.

unsigned char rammap[DRIVER_RAMMAP_SIZE];
unsigned char screen[DRIVER_REFTABLE_SIZE];




//Some high-level helpful functions.
static unsigned char mapDraw(signed int x, signed int y, PGM_P map, unsigned char width, unsigned char height);
static unsigned char RenderMap( signed int x, signed int y );
static void DrawFrame( unsigned char x, unsigned char y, unsigned char width, unsigned char height, unsigned char frame );
static void DrawInt16( int x, int y, unsigned int i );




#define MARIO_WIDTH	16
#define MARIO_HEIGHT 16


static inline void demo2()
{
	unsigned int i;

	driver_fill(128);
	wait_seconds(1);

	unsigned char x1=50, x2=60, x3=70, x4=80, x5=40, y1=20, y2=80, y3=78,y4=65,y5=45;
	signed char dx1=1, dx2=-1, dx3=1, dx4=-1, dx5=1, dy1=-1, dy2=1, dy3=1, dy4=-1, dy5=1;

	driver_fill(22);
	//background_draw(1,5,avgalogo,11,3);
	wait_seconds(1);

	unsigned long l=0;

	while(1)
	{
		if(i >= 60)
		{
				int tmp = (l*7) / 1000;

				DrawInt16(4, 10, tmp / 1000); //show remaining time
				driver_print_C(5,10,".");
				DrawInt16(6, 10, tmp % 1000);
				driver_print_C(2,12,"MIPS left");

				l=0;
				i=0;
		}

		overlay_clear();
		overlay_draw_sprite(x1, y1, 1);
		overlay_draw_sprite(x2, y2, 2);
		overlay_draw_sprite(x3, y3, 3);
		overlay_draw_sprite(x4, y4, 4);


		x1+=dx1; if(x1 >= DRIVER_RESX-8 || x1 <= 0) dx1=-dx1;
		y1+=dy1; if(y1 >= DRIVER_RESY-8 || y1 <= 0) dy1=-dy1;

		x2+=dx2; if(x2 >= DRIVER_RESX-8 || x2 <= 0) dx2=-dx2;
		y2+=dy2; if(y2 >= DRIVER_RESY-8 || y2 <= 0) dy2=-dy2;

		x3+=dx3; if(x3 >= DRIVER_RESX-8 || x3 <= 0) dx3=-dx3;
		y3+=dy3; if(y3 >= DRIVER_RESY-8 || y3 <= 0) dy3=-dy3;

		x4+=dx4; if(x4 >= DRIVER_RESX-8 || x4 <= 0) dx4=-dx4;
		y4+=dy4; if(y4 >= DRIVER_RESY-8 || y4 <= 0) dy4=-dy4;

		x5+=dx5; if(x5 >= DRIVER_RESX-8 || x5 <= 0) dx5=-dx5;
		y5+=dy5; if(y5 >= DRIVER_RESY-8 || y5 <= 0) dy5=-dy5;
		
		
		// sync(); sync(); sync(); sync(); sync(); sync(); sync(); sync(); sync(); sync(); sync(); sync(); sync();

		l += sync();

		i++;
		
	}
}


const unsigned char text1[] PROGMEM ="RETROwIZ! AVGA 2.0-based retro PC. The ATmega328 runs at 32MHz and produces 168*200 progressive VGA signal with 16 colors. ";


/*
left:   107
right:  116
top:    117
bottom: 114

right ctrl: 116
right alt:  17

left ctrl: 20
left alt:  17


*/


WINDOW wnd2;


#define KEY_LEFT	(1<<0)
#define KEY_RIGHT       (1<<1)
#define KEY_UP          (1<<2)
#define KEY_DOWN        (1<<3)

unsigned char kkk;
unsigned char status=0, event=0;
void onKbEvent(unsigned char code, unsigned char flags)
{
        //special key?
	if(flags & 1) //printf_P(PSTR("Special %d\n"),code);
		switch(code)
		{
			case 107: if(flags&2) status &= ~KEY_LEFT;  else status |= KEY_LEFT;  break;
			case 116: if(flags&2) status &= ~KEY_RIGHT; else status |= KEY_RIGHT; break;
			case 117: if(flags&2) status &= ~KEY_UP;    else status |= KEY_UP;    break;
			case 114: if(flags&2) status &= ~KEY_DOWN;  else status |= KEY_DOWN;  break;
		}
	else if(!(flags&2))
	{
		kkk = keyboard_scan2key(code);
		//printf_P(PSTR("Key: %d\n"), key);
	}

	static unsigned char last_status;
	event = status & (~last_status);
	last_status = status;
}




extern unsigned int c0;


int main( void )
{
	//map memory for video driver
	driver_mmap(screen, pgmmap, rammap);

	//initialization
	sound_init();
	video_init(VGA2);
	//input_init();

	enable(); 			//start engine.
	


	wait_seconds(1); 	//wait for display to sync.

	sync();
	sync();
//	sound_play(music1);
	//sound_stop();

	keyboard_init(onKbEvent);



	console_init_stdio();

	SD_Init();


        unsigned char res = SD_Reset();
	printf_P(PSTR("\nSD Reset: %d\n\n"), res);
	wait_seconds(1);


	unsigned int sector=0;


	/*while(1)
	{
		unsigned char buf[10];
		unsigned int lastc0 = c0;
		unsigned char res = SD_ReadSector(sector^0x5555, buf);

		//res = SD_ReadSector(sector, buf);
		//res = SD_ReadSector(sector, buf);
		//res = SD_ReadSector(sector, buf);


		printf_P(PSTR("\n\nSector %d: %d\n"), lastc0, c0);  //1016 cyklu na linku
		char i;
		//for(i=0;i<10;i++) printf("%02X ",buf[i]);
                //wait_seconds(1);
                sector++;

                sync();
                //sync();
	}*/



        /*while(1)
	{
         	video_sync();
        	keyboard_handle();
	}/**/

//	memset(ptrtmp,0,sizeof(ptrtmp));



//while(TCNT1 < 100);

	driver_fill(0);
	wait_seconds(1);
	driver_print_C(3, 1,  "RetroWiz DEMO");
	driver_print_C(2, 4,  "AVR SuperMario");
	driver_print_C(2, 6,  "==============");
	driver_print_C(2, 8,  "(AlphaVersion)");
	driver_print_C(3, 16, "PRESS ANY KEY");
	wait_seconds(2);


//	while(1);

	//overlay_draw_sprite(3,5,1);

	/*key_get_event();
	while(!key_get_event()) sync();*/



	//demo2();


	unsigned char y1=25, y2=25, xs=50,ys=50;
	signed char dy1=1,dy2=-1,dxs=1,dys=1;




	driver_fill(128);
	WINDOW wnd0 = window_add_standard(0, 2);
	WINDOW wnd1 = window_add_standard(3, DRIVER_HEIGHT-5);
	wnd2 = window_add_standard(DRIVER_HEIGHT-1, 3); //leave one row for scroll.

	window_select(wnd2);
	DrawFrame(0, 0, DRIVER_WIDTH-2, 1, 24); //5
	//window_print_P(1, 1, PSTR("   Hello, world!"));




	char tttt=0;
	char ttttt=0;

	unsigned char g=1, a=1, j=0,  jump=0, air=0, u=1;
	signed char ldx, vx=0;

	signed char dx=0, dy=0;
	unsigned int x=100, y=100; 		//x a y jsou souradnice postavy vzhledem k levemu hornimu okraji mapy. VZDY
	signed int xm=0, ym=0;

	PGM_P frame = anim1; //animation

	int tim1=100;





	int xxx=DRIVER_WIDTH*8;
	char yyy=0,dyy=1;
	unsigned char ccc=0;
	while(1)
	{
		window_select(wnd0);
		mapDraw( xxx>>2, yyy, text1, strlen_P(text1), 1 );

		if(++ttttt>8)
		{
			ttttt=0;
			yyy+=dyy; if((yyy>=7)||(yyy<=0)) dyy=-dyy;
		}

		xxx-=2;
		//if(-xxx > (strlen_P(text1)*8)) xxx=DRIVER_WIDTH*8;

		window_set_colormask((ccc++) | 0x80);




		//render();
		window_select(wnd1);

		if(RenderMap(xm, ym))
			overlay_release();
		else
			overlay_clear();
		
/*		sync();
		xm++;
		continue;   */


		//overlay_draw(xb, yb, frame, 16, 16); //nebo: xs, ys


		unsigned int xb = x+xm, yb = y+ym; //poloha postavy na obrazovce.

		overlay_draw_param(xb, yb, frame, 16, 16, ldx < 0 ? OVERLAY_HFLIP : 0); //nebo: xs, ys

                //overlay_draw_sprite(0, 0, 1);
		overlay_draw_sprite(xs, ys, 1);
		xs+=dxs; if(xs >= DRIVER_RESX-8 || xs <= 0) dxs=-dxs;
		ys+=dys; if(ys >= window_get_num_lines()-8 || ys <= 0) dys=-dys;



	//	overlay_draw_sprite_param(48, +y1, 'A', 8);
		overlay_draw_sprite_param(64, +y2, 'V', 8|1);
		overlay_draw_sprite_param(80, +y1, 'G', 8|2);
		overlay_draw_sprite_param(96, +y2, 'A', 8|3);
		if(tttt)
		{
		 y1+=dy1; if(y1 >= 90) { y1=90; dy1=-dy1; } else dy1++;
		 y2+=dy2; if(y2 >= 90) { y2=90; dy2=-dy2; } else dy2++;
		}
		tttt=!tttt;




		/*memset(rammap,0x55, sizeof(rammap));

		char i,k,c;
		int j=0;
		for(k=0;k<16;k++)
		{
                  	c++;
        		c&=0x0F;
        		c|=1;

			//for(i=0;i<32;i++) rammap[j++]=(c)|(c<<4);
        		window_set_block(1+k,5,DRIVER_PGM_BLOCK_COUNT+k);



  		}    */




    	//frameMove();

		//user input
		//unsigned char event = key_get_event();
		if(event & KEY_UP && !air) { sound_play(snd_jump); jump = 1; dy = -3; }
		if(event & KEY_LEFT)  a=1;
		if(event & KEY_RIGHT) a=1;


		//unsigned char status = key_get_status();
		vx=0;

		if(status & KEY_RIGHT) vx+=1; //desired speed
		if(status & KEY_LEFT)  vx-=1; //desired speed
		if(status & KEY_DOWN)  ym--;


		//if(!--tim1) {tim1=50; if(!air) {  jump=1; dy=-3;  sound_play(snd_jump);}    }
		//vx+=1; //desired speed





		if(vx) ldx=vx;

		if(!--a)
		{
			//dx+=sgn(vx-dx);
			if(vx>dx)dx++;
			if(vx<dx)dx--;
			a=5; 				//1/acceleration of Mario.
		}
		

		//collision engine.

		//note that absolute value of dx and dy must always be lower or equal to 8.
		//otherwise the collision detector would be much more complicated to work properly.
		//ziskat kolizni blok. bereme ze SOUCASNE VIDITELNE MAPY, dopocitavame absolutni polohu.
		//#define window_coords2block() ...
		//xb a yb jsou ted absolutni souradnice leveho horniho bloku MARIA NA OBRAZOVCE.


		xb += window_get_scrollX();
		yb += window_get_scrollY();  //window_get_startline();
		xb >>= 3;
		yb >>= 3;

		unsigned char colwidth = 2;
		unsigned char colheight = 2;
		if(x&7) colwidth++;
		if(y&7) colheight++;


vert:	if(dy > 0)  //dole
		{
				   y--;
				   unsigned char pre = y&0xF8;
				   y += dy;
				   unsigned char post = y&0xF8;
				   y++;
				   if(post == pre) goto horiz;

				   unsigned char k = yb + colheight;
				   unsigned char l = xb;
				   for(j=0;j<colwidth;j++) 					//projet radek.
				   {
					 //unsigned char b=window_get_block(l++,k);
                                         unsigned char b=overlay_get_background_block(window_get_block_ptr(l++,k));


					 if(b > 212 && b < 217 || b == 222 || b == 150 || b == 153) 		//optimalizovat pomoci pointeru na block
					 {
					  	dy=0;

						if(air) sound_play(snd_impact);
						air = 0; 		  //muzeme skocit?
						jump = 0;		  //pro animaci
						g = 1;			  //gravity timer
						if(b==222) { sound_play(snd_tram); dy=-6;} //trampolina

						//y &= 0xFFF8;
						asm volatile("mov %A0, %1" : "=r" (y) : "r" (post));
						goto horiz;
					 }
				   }
				   //k vertikalni kolizi nedoslo - posunout blok pro horizontalni kolizi
				   //yb++;
				   colheight++;
		}
	    else if(dy < 0) //nahore
		{
				  unsigned char pre = y&0xF8;
				  y += dy;
				  unsigned char post = y&0xF8;
				  if(post == pre) goto horiz;

				  unsigned char k = yb - 1;
				  unsigned char l = xb;

				  for(j=0;j<colwidth;j++) 						//projet radek.
				  {
				    	//unsigned char b=window_get_block(l++,k);
					unsigned char b=overlay_get_background_block(window_get_block_ptr(l++,k));
					if(b > 212 && b < 217) 
					{ 
						dy = 0; 
						asm volatile("mov %A0, %1" : "=r" (y) : "r" (pre));
						goto horiz; 
					}
				  }

				  //k vertikalni kolizi nedoslo - posunout blok pro horizontalni kolizi
				  yb--;
				  colheight++;
		}
horiz: 	if(dx > 0) //vpravo
		{
				  x--;
				  unsigned char pre = x&0xF8;
				  x += dx;
				  unsigned char post = x&0xF8;
				  x++;
				  if(post == pre) goto next;

				  unsigned char k = xb + colwidth;
				  unsigned char l = yb;
				  for(j=0;j<colheight;j++) 					//projet sloupecek.
				  {
				 	//unsigned char b=window_get_block(k,l++);
				 	unsigned char b=overlay_get_background_block(window_get_block_ptr(k,l++));
					if(b > 212 && b < 217)
					{
						dx = 0; 
						//x &= 0xFFF8;
						asm volatile("mov %A0, %1" : "=r" (x) : "r" (post));
						goto next;
					}
				  }
		}
		else if(dx < 0) //vlevo
		{
				  unsigned char pre = x&0xF8;
				  x += dx;
				  unsigned char post = x&0xF8;
				  if(post == pre) goto next;

				  unsigned char k = xb - 1;
				  unsigned char l = yb;
				  for(j=0;j<colheight;j++) 					//projet sloupecek.
				  {
				  	//unsigned char b=window_get_block(k,l++);
				  	unsigned char b=overlay_get_background_block(window_get_block_ptr(k,l++));
					if(b > 212 && b < 217) 
					{
					 	dx = 0; 
						asm volatile("mov %A0, %1" : "=r" (x) : "r" (pre));
						goto next;
					}
				  }
		}

next:	if(dy>0 && !air) sound_play(snd_fall);
		if(dy) air=1;
		


		//gravitace, je vyhodnejsi tady nez nahore.
		if(!--g) 
		{
			if(dy<8) dy++;
			g=5;			//1/acceleration of Earth
		}

		//animace postavy. TODO:!!!!!
		if(!--u)
		{
			u=4;
			frame += 128;
			if((unsigned int)frame >= (unsigned int)anim1+(128*5))  frame = anim1 + 128;
		}
		if(!dx) frame = anim1;
		if(abs(dx-vx) > 1) frame = anim1 + 128*5;
		if(air) frame = anim1 + 128*4;
		if(jump) frame = anim1 + 128*6;


		//if(dx && !air) { aaa++; if((aaa&7) == 0) sound_play(aaa&8 ? snd_walk1:snd_walk2);  }



		//pohyb mapy vzhledem k postave. exponenciala
		signed int tmp2 = (((DRIVER_RESX/2)-(MARIO_WIDTH/2)))   - x;
		xm += (tmp2 - xm)/8;

   	    	tmp2 = (( (window_get_num_lines()/2 + 24) -(MARIO_HEIGHT/2)))  - y;   // ... pridano -16 pro 144x144

		ym += (tmp2 - ym)/8;
		if(tmp2 - ym < -32) ym += (tmp2-ym) + 32;




		//ok, wait for retrace.
		window_select(wnd2);
		static char kbtext=0;

		if(!kbtext)
		{
	 	 window_print_P(1, 1, PSTR("x=      y=       "));
		 DrawInt16(DRIVER_WIDTH-18, 1, x); //show remaining time
		 DrawInt16(DRIVER_WIDTH-10, 1, y); //show remaining time
		}


		if(kkk)
		{
                  if(!kbtext) window_fill(128);
                  kbtext=1;
                  putch(kkk);
                  kkk=0;
  		}                

		/*
		window_print_P(1, 1, PSTR("x=       y=         "));
		DrawInt16(3, 1, x); //show remaining time
		DrawInt16(12, 1, y);
		*/

		//while(1);
		
		//char buf[10];
		//unsigned char res = SD_ReadSector(sector++, buf);
		//res = SD_ReadSector(sector++, buf);
		//res = SD_ReadSector(sector++, buf);
		//res = SD_ReadSector(sector++, buf);


		sync();
	}/**/
}







static void DrawInt16(int x, int y, unsigned int i)
{
	unsigned char str[6];
	itoa(i,str,10);
	window_print(x, y, str);
}

static void DrawFrame(unsigned char x, unsigned char y, unsigned char width, unsigned char height, unsigned char frame)
{
	unsigned char i;

	window_set_block(x++, y, frame++);
	for(i=width; i;i--) window_set_block(x++, y, frame); frame++;
	window_set_block(x, y++, frame++);
	for(i=height;i;i--) window_set_block(x, y++, frame); frame++;
	window_set_block(x--, y, frame++);
	for(i=width; i;i--) window_set_block(x--, y, frame); frame++;
	window_set_block(x, y--, frame++);
	for(i=height;i;i--) window_set_block(x, y--, frame); frame++;
}





static unsigned char RenderMap(signed int x, signed int y)
{
    	x+=7;
	y+=7;

	window_adjust_for_background(x, y); 	//adjust window
	x>>=3;
	y>>=3;

	static signed char lasttmpx, lasttmpy;
	if(x == lasttmpx && y == lasttmpy) return 0; //no need to redraw!
	lasttmpx=x;
	lasttmpy=y;

	//now draw game map and everything attached to it.
	window_fill_scroll(128);


	background_draw( x+64*0, y, map1, 64, 30 );
	background_draw( x+64*1, y, map1, 64, 30 );
	background_draw( x+64*2, y, map1, 64, 30 );
	background_draw( x+64*3, y, map1, 64, 30 );



	//background_draw( x+30, y+20, avrlogo, 4, 2);

	return 1;
}




static unsigned char mapDraw(signed int x, signed int y, PGM_P map, unsigned char width, unsigned char height)
{
    	x+=7;
	y+=7;

	window_adjust_for_background(x, y); 	//adjust window
	x>>=3;
	y>>=3;

	static signed char lasttmpx, lasttmpy;
	if(x == lasttmpx && y == lasttmpy) return 0; //no need to redraw!
	lasttmpx=x;
	lasttmpy=y;

	//now draw game map and everything attached to it.
	window_fill_scroll(128);

	background_draw( x, y, map, width, height );

	//background_draw( x+30, y+20, avrlogo, 4, 2);

	return 1;
}
