/*
 *  Albert Seward Copyright (C) 2005
 *  All Rights Reserved. Proprietary.
 *  alse7905@student.uu.se
 * 
 */

#include <string.h>

#include "../AVGA/avga.h"

#include "misc.h"

#include "pacman.h"
#include "highscore.h"
#include "scene.h"
  
void scene_intro(void) {
	uint8_t  i, mode, tmp;
	int pos, pacman;
	
	while(key_read(KEY_ANY))  sync();

	for(;;) {
		sprite_init();
		          
		mode0_cls();

		mode0_print(5,1, "\033\021\021\021\021\021\021\021\032");
		mode0_print(5,2, "\020" PACMAN_LOGO "\020");
		mode0_print(5,3, "\035\021\021\021\021\021\021\021\034");


		mode0_print(4,19, "INSERT COIN");
		mode0_print(3,22, "COPYRIGHT 2005");
		mode0_print(3,23, "ALBERT SEWARD");



		pos = 60;
		pacman = 70;

		mode = 1;

		for(;;) 
                {

			if(key_read(KEY_COIN))
				return;

			for(i = 1; i < 5 /*SPRITE_COUNT*/; i++)
                        {
				tmp = pos - i * 10;
				if(tmp < 152 && tmp > 0)
                                {
					if(mode)
						sprite_put(5-i, tmp, 70, BLINKY -1 + i);
					else if(pacman < tmp)
						sprite_put(5-i, tmp, 70, EATEN);
					else
						sprite_put(5-i, tmp, 70, EATABLE);
				}
			}

			if(pacman < 152 && pacman > 0)
				sprite_put(0, pacman, 70, pacman & 8 ? (mode?PACMAN_OPEN_RIGHT:PACMAN_OPEN_LEFT) : (mode?PACMAN_CLOSED_RIGHT:PACMAN_CLOSED_LEFT));


			if(mode) 
                        {
				pos++;
				pacman++;
			} 
                        else
                        {
				pos--;
				pacman-=2;
			}

		if(pos > 152)
			pacman = 200;
		else if(pos < -10)
			pacman = 0;


			if(pos > 250) 
                        {
				mode = 0;
			} 
                        else if(pos < -40) 
                        {
				mode0_print(5,6, "\045  BLINKY");
				key_wait_timeout(KEY_COIN, 60);
				mode0_print(5,7, "\046  CLYDE");
				key_wait_timeout(KEY_COIN, 60);
				mode0_print(5,8, "\047  PINKY");
				key_wait_timeout(KEY_COIN, 60);
				mode0_print(5,9, "\050  INKEY");
				key_wait_timeout(KEY_COIN, 200);
				memset(&mode0_get_block(5,6), ' ', 20*4);
				break;
			}


			sync();
			sync();
			sprite_clear();

		}

		highscore_view();
	}
}
