/*
 *  Albert Seward Copyright (C) 2005
 *  All Rights Reserved. Proprietary.
 *  alse7905@student.uu.se
 * 
 */

#ifndef _MISC_H_
#define _MISC_H_

#include <inttypes.h>
#include <avr/io.h>

#define NCLOSED 0xff     /* Normally closed, (STK500) */
#define NOPEN   0x00     /* Normally open, (joystick) */

#define LED_PORT PORTB
#define LED_DDR  DDRB

#define LED_MODE NCLOSED

#define KEY_PORT PORTC
#define KEY_PIN  PINC
#define KEY_DDR  DDRC

#define KEY_MODE    NCLOSED



#define KEY_RIGHT   0x01
#define KEY_LEFT    0x02
#define KEY_UP      0x04
#define KEY_DOWN    0x08

#define KEY_COIN    0x10
#define KEY_START   0x20



#define KEY_DIR (KEY_LEFT | KEY_RIGHT | KEY_UP | KEY_DOWN)
#define KEY_ANY (KEY_DIR | KEY_COIN | KEY_START)

void utoa10(uint16_t num, uint8_t *str);
void key_init(void);
uint8_t key_wait_timeout(uint8_t mask, uint8_t timeout);
uint8_t misc_rand(void);
#define EEPROM_ERROR      1
uint8_t boot_test(void);




//#define key_read(mask)  ((KEY_PIN ^ KEY_MODE) & (mask))

extern unsigned char status;
#define key_read(mask)  ({ ((status) & (mask)); })


#define delay(x) {uint8_t __i; for(__i = 0; __i < (x); __i++) sync(); }


#endif /* _MISC_H_ */
