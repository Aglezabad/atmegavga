#include <stdio.h>
#include "graphics.h"

main()
{
  unsigned char aa[]=GRAPHICS;
  
  FILE *f=fopen("graphics12.h","w");

  int i, j=0;
  fprintf(f, "#define GRAPHICS { \\ \n");

  for(i=0;i<sizeof(aa);i++)
  {

    unsigned char tmp = ((aa[i]&0xF0)>>4) | ((aa[i]&0x0F)<<4);

    fprintf(f, "0x%02x, ", tmp);

    if(++j >= 4) {    fprintf(f, " \\ \n"); j=0;}

  }

   fprintf(f, "}\n");
}