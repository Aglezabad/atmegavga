/*
 *  Albert Seward Copyright (C) 2005
 *  All Rights Reserved. Proprietary.
 *  alse7905@student.uu.se
 * 
 */

#include <avr/eeprom.h>
#include <string.h>

#include "../AVGA/avga.h"
#include "misc.h"

static unsigned int _rand = 1;

uint8_t misc_rand() {
	return ((_rand = _rand * 1103515245L + 12345L) % 255);
}
	
void utoa10(uint16_t num, uint8_t *str) {
	
	uint16_t tmp;
	
	*(str++) = '0';
	*(str++) = '0';
	*(str++) = '0';
	*(str++) = '0';
	*(str) = '0';
	
	do {
		tmp = num / 10;
		*(str--) = '0' + num - tmp * 10;
		num = tmp;
	} while(num);
	
}

void key_init(void) { 
 	KEY_DDR &= ~KEY_ANY;
	KEY_PORT |= KEY_ANY;
}

uint8_t key_wait_timeout(uint8_t mask, uint8_t timeout) {
	uint8_t i;
	for(i = 0; (i < timeout) && !key_read(mask); i++) {
		sync();
		sync();
	}
	return i != timeout;
} 

#define EEPROM_MAGIC 0x3f81

/*
 * TODO: write flash test
 */ 
uint8_t boot_test(void) {
	uint8_t j,k;
	
	mode0_cls();
	mode0_print(1,1, "FLASH");
	mode0_print(1,2, "EEPROM");
	mode0_print(1,4, __DATE__);
	mode0_print(1,5, __TIME__);
	mode0_get_block(2,4) &= 0xdf;
	mode0_get_block(3,4) &= 0xdf;
	

        //nakreslit 4*4bloky z pointeru nekam do flash (sum)
	//for(j = 0; j < 64; j++)
	//	mode0_set_block(1+(j&0x03),14+(j>>3),j+96);

	for(j=0;j<32;j++)
	 for(k=0;k<40;k++)
	  overlay_putpixel(j+16,k+120,misc_rand());



	mode0_print(8,1, "OK");
	
	while(!eeprom_is_ready());
	if(eeprom_read_word((uint16_t *)(E2END-4)) != EEPROM_MAGIC) {
		mode0_print(8,2, "FAIL");
		while(!eeprom_is_ready());		
		eeprom_write_word((uint16_t *)(E2END-4), EEPROM_MAGIC);		
		return EEPROM_ERROR;
	} else {
		mode0_print(8,2, "OK");
	}
	
	return 0;
}
