/*
 *  Albert Seward Copyright (C) 2005
 *  All Rights Reserved. Proprietary.
 *  alse7905@student.uu.se
 * 
 */

#ifndef _HIGHSCORE_H_
#define _HIGHSCORE_H_

void highscore_reset(void);
void highscore_view(void);
void highscore_enter(uint16_t score);		

#endif /* _HIGHSCORE_H_ */
