/*
 *  Albert Seward Copyright (C) 2005
 *  All Rights Reserved. Proprietary.
 *  alse7905@student.uu.se
 * 
 */

#include <avr/eeprom.h>
#include <string.h>

#include "../AVGA/avga.h"

#include "pacman.h"
#include "misc.h"
#include "highscore.h"

typedef struct {
	char name[3];
	uint16_t score;			
} entry_t;

void highscore_reset(void) {
	uint8_t j;	
	entry_t e;
	
	e.name[0] = 'P';
	e.name[1] = 'A';
	e.name[2] = 'C';
	
	for(j = 0; j < 15; j++) {
		e.score = (15-j)*1000;
		while(!eeprom_is_ready());
		eeprom_write_block(&e, (void *)(j*5), 5);		
	}	
}

static void highscore_draw(void) {
	uint8_t i,j;
	
	memset(_ramp, 0, 80);
	
	for(j = 0; j < 19; j++) {
		for(i = 0; i < 12; i++) {
			mode0_set_block(j,i*2, mode0_rammap(0));
			mode0_set_block(j,i*2+1, mode0_rammap(1));
		}
		mode0_set_block(j, 0, 0x11);
		mode0_set_block(j, 23, 0x11);
	} 
	
	mode0_print(4,4, "\020HIGH SCORE\020");	
	
	for(i = 0; i < 10; i++) {
		mode0_set_block(5+i,5, '\021');
		mode0_set_block(5+i,3, '\021');
		mode0_set_block(5+i,21, '\021');
	}
	mode0_set_block(4,3, '\033');
	mode0_set_block(4,5, '\027');
	mode0_set_block(4,21, '\035');
	mode0_set_block(15,3, '\032');
	mode0_set_block(15,5, '\030');
	mode0_set_block(15,21, '\034');
		
	for(i = 0; i < 15; i++) {	
		mode0_set_block(4,6+i, '\020');
		for(j = 0; j < 10; j++)
			mode0_set_block(5+j,6+i, ' ');	
		mode0_set_block(15,6+i, '\020');
	}
	
}

static inline void highscore_save(entry_t *list) {
	while(!eeprom_is_ready());
	eeprom_write_block(list, NULL, 5*15);		
}

static entry_t *highscore_read(void) {

	entry_t *entry = (entry_t *)(_ramp+80);  // Use sprite memory 
	
	while(!eeprom_is_ready());
	eeprom_read_block(entry, NULL, 5*15);		
	       
	return entry;
}

void highscore_enter(uint16_t score) {	
	entry_t *list;	
	uint8_t scroll, i, pos, key, key_old;
	int8_t score_pos;
	
	list = highscore_read();
	highscore_draw();	
	
	// Insert new highscore
        if(list[14].score <= score) {
		for(score_pos = 14; score_pos >= 0 && list[score_pos].score <= score; score_pos--)
			memcpy(&list[1+score_pos], &list[score_pos], 5);
		score_pos++;
	} else score_pos = 15;	
	
	list[score_pos].score = score;	
	list[score_pos].name[0] = 'A';
	list[score_pos].name[1] = 'A';
	list[score_pos].name[2] = 'A';
	
	
	scroll = 0;
	pos = (score_pos == 15)?3:0;
	key_old = 0;
	while(1) {
	
		scroll++;
		if(scroll == /*20*/16)
			scroll = 0;
       
		key = key_read(KEY_ANY);
		
		if(key != key_old) {
			key_old = key;
			switch(key) {
			case KEY_DOWN:
				
				if(list[score_pos].name[pos] < 'Z')
					list[score_pos].name[pos]++;
				break;	
			case KEY_UP:
				if(list[score_pos].name[pos] > 'A')
					list[score_pos].name[pos]--;
				break;			
			case KEY_LEFT:
				if(pos > 0) pos--;			
				break;
			case KEY_RIGHT:
			case KEY_START:
				pos++;
				if(pos > 2) {
					if(score_pos != 15) {
						mode0_cls();
						highscore_save(list);
					}
					return;
				}
			}			
		}
		
		for(i = 0; i < 15; i++) { 	
			mode0_set_block(5,6+i, list[i].name[0]);
			mode0_set_block(6,6+i, list[i].name[1]);
			mode0_set_block(7,6+i, list[i].name[2]);
			utoa10(list[i].score, &mode0_get_block(10,6+i));
		}

		if(score_pos < 15)
			mode0_set_block(5+pos,6+score_pos, (scroll & 8)?list[score_pos].name[pos]:' ');

		_ramp[scroll*4+1] = 0x05;
		sync();
		sync();
		_ramp[scroll*4+1] = 0;
	}
}
	

void highscore_view(void) {
	entry_t *list;	
	uint8_t scroll, i, timer;
	
	list = highscore_read();
	highscore_draw();	
	
	scroll = 0;
	i = 0;
	timer = 0;
	do {
		scroll++;
		if(scroll == /*20*/ 16) {
			scroll = 0;
			i++;
			timer++;
		}
	
		if(i < 15) {
			mode0_set_block(5,6+i, list[i].name[0]);
			mode0_set_block(6,6+i, list[i].name[1]);
			mode0_set_block(7,6+i, list[i].name[2]);
			utoa10(list[i].score, &mode0_get_block(10,6+i));
		}

		_ramp[scroll*4+1] = 0x05;
		sync();
		sync();
		_ramp[scroll*4+1] = 0;
	} while(!key_read(KEY_COIN) && timer < 25);	
		
}
